package com.android.ranit.beatmonitor.common;

/**
 * BicycleState 类为健身车指令类
 **/

public class BicycleState
{
	public static byte ORDER_START =0x02;//起始位
	// UartCmd
	public static byte CarTableModel   = 0x50;   // 配置设备型号：0x02 0x50 0x00 0x50 0x03  可以获取设备品牌、机型
	public static byte CarTableParam   = 0x41;   // 设备参数
	public static byte CarTableStatus  = 0x42;   // 设备状态
	public static byte CarTableData    = 0x43;   // 设备数据
	public static byte CarTableControl = 0x44;   // 设备控制

	// 获取设备信息
	public static byte CarTableParamInfo    = 0x02;  // 获取0x02 0x41 0x02 0x40 0x03：  阻力B  坡度B  配置B  段数B
	public static byte CarTableParamTotal   = 0x03;  // 获取0x02 0x41 0x03 0x42 0x03：  累计值
	public static byte CarTableParamDate    = 0x04;  // 同步时间：传入数据  年月日周时分秒 年传入后2位
	// 获取设备状态(读取), 返回值
	public static byte CarTableParamStatusNormal     = 0;   // 待机
	public static byte CarTableParamStatusStarting   = 1;   // 启动中
	public static byte CarTableParamStatusRunning    = 2;   // 运行   数据：速度W 阻力B 频率W 心率B 瓦特W 坡度B 段索引B
	public static byte CarTableParamStatusPause      = 3;   // 暂停   数据：速度W 阻力B 频率W 心率B 瓦特W 坡度B 段索引B
	public static byte CarTableParamStatusSleep      = 20;   // 睡眠
	public static byte CarTableParamStatusError      = 21;   // 故障

	// 车表数据
	public static byte CarTableDataSportData      = 0x01;  // 获取0x02 0x43 0x01 0x43 0x03 ：  时间W 距离W 热量W 计数W
	public static byte CarTableDataSportInfo      = 0x02;  // 获取0x02 0x43 0x02 0x41 0x03：  用户L 运动L 模式B 段数B 目标W
	public static byte CarTableDataProgramData    = 0x03;  // 获取：  索引B 数据N  这个可能不会用到

	// 设备控制(写入)
	/*app在即将开始运行设备时，最先发送此指令给设备，通知设备即将开始，设备接收到此指令后，进行运动数据重置，同事恢复相关设置值   备注：倒计值 若设备需要倒计时提示用户，返回数据为倒计秒值，若不需要返回0*/
	public static byte CarTableControlReady             = 0x01;   // 0x02 0x44 0x01 0x45 0x03 准备就绪
	public static byte CarTableControlStart             = 0x02;   // 0x02 0x44 0x02 0x46 0x03 开始继续
	public static byte CarTableControlPause             = 0x03;   // 0x02 0x44 0x03 0x47 0x03 暂停
	public static byte CarTableControlStop              = 0x04;   // 0x02 0x44 0x04 0x40 0x03 停止
	public static byte CarTableControlParam             = 0x05;   // 设置参数  设置阻力  坡度
	public static byte CarTableControlStep              = 0x06;   // 设置步进  设置阻力  坡度
	public static byte CarTableControlUser              = 0x0A;   // 写入用户数据 ID（L） 体重B  身高B 年龄B 性别B（0:男  1:女）
	public static byte CarTableControlSportMode         = 0x0B;   // 运动模式  运动ID（L） 模式B   段数B  目标W
	public static byte CarTableControlFunctionSwitch    = 0x0C;   // 功能关开
	public static byte CarTableControlProgram           = 0x0D;   // 设置程式模式

	// 设备控制（启动时的模式）
	public static byte CarTableStartModeFree          = 0x00;  // freedom mode
	public static byte CarTableStartModeTime          = 0x01;  // time mode
	public static byte CarTableStartModeDistance      = 0x02;  // distance mode
	public static byte CarTableStartModeCalory        = 0x03;  // calories mode
	public static byte CarTableStartModeCount         = 0x04;  // counting mode
	public static byte CarTableStartModeResistance    = 0x10;  // resistance control mode
	public static byte CarTableStartModeHeartRate     = 0x20;  // heart rate control mode
	public static byte CarTableStartModeWatt          = 0x03;  // watt control mode

}
