package com.android.ranit.beatmonitor.view.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.ranit.beatmonitor.R;
import com.android.ranit.beatmonitor.common.RowerConnectionStates;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BleDeviceAdapter extends RecyclerView.Adapter<BleDeviceAdapter.ViewHolder> {

    private static final String INITIAL_ANIMATION = "connect.json";
    private static final String CONNECTED = "done.json";
    private static final String CONNECTING = "connecting.json";
    private static final String DISCONNECT = "cancel.json";

    private Context mContext;
    private List<BluetoothDevice> mDeviceList;
    private HrmDeviceItemClickListener mClickListener;
    private RowerConnectionStates mCurrentState = RowerConnectionStates.DISCONNECTED;
    private int mSelectedPosition = -1;

    // Click listener Interface
    public interface HrmDeviceItemClickListener {
        void onHrmDeviceClicked(int position);
    }

    // Constructor
    public BleDeviceAdapter(Context context, List<BluetoothDevice> deviceList,
                            HrmDeviceItemClickListener clickListener) {
        this.mContext = context;
        this.mDeviceList = deviceList;
        this.mClickListener = clickListener;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.hrm_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BleDeviceAdapter.ViewHolder holder, int position) {
        BluetoothDevice currentDevice = mDeviceList.get(position);
        holder.tvDeviceName.setText(currentDevice.getName());
        holder.tvDeviceAddress.setText(currentDevice.getAddress());
        prepareLottieAnimationView(holder.lottieConnectivityStatus, INITIAL_ANIMATION,true);

        holder.layoutHrmItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onHrmDeviceClicked(position);
            }
        });

        // Customize UI
        holder.layoutHrmItem.setClickable(true);
        if (mSelectedPosition == position) {
            if (mCurrentState == RowerConnectionStates.DISCONNECTED) {
                holder.tvDeviceName.setText(currentDevice.getName());
                prepareLottieAnimationView(holder.lottieConnectivityStatus,
                        DISCONNECT, false);
            } else if (mCurrentState == RowerConnectionStates.CONNECTED) {
                String text = "Connected to "+currentDevice.getName();
                holder.tvDeviceName.setText(text);
                prepareLottieAnimationView(holder.lottieConnectivityStatus,
                        CONNECTED, false);
            } else if (mCurrentState == RowerConnectionStates.CONNECTING) {
                holder.layoutHrmItem.setClickable(false);
                prepareLottieAnimationView(holder.lottieConnectivityStatus,
                        CONNECTING, true);
            } else if (mCurrentState == RowerConnectionStates.DISCONNECTING) {
                String text = "Disconnecting from "+currentDevice.getName();
                holder.tvDeviceName.setText(text);
            }
        } else {
            prepareLottieAnimationView(holder.lottieConnectivityStatus,
                    INITIAL_ANIMATION, true);
        }
    }

    @Override
    public int getItemCount() {
        return mDeviceList.size();
    }

    // View-Holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout layoutHrmItem;
        TextView tvDeviceName;
        TextView tvDeviceAddress;
        LottieAnimationView lottieConnectivityStatus;

        public ViewHolder(@NotNull View itemView) {
            super(itemView);

            layoutHrmItem = itemView.findViewById(R.id.layoutHrmItem);
            tvDeviceName = itemView.findViewById(R.id.tvDeviceName);
            tvDeviceAddress = itemView.findViewById(R.id.tvDeviceAddress);
            lottieConnectivityStatus = itemView.findViewById(R.id.lottieStatus);
        }
    }

    /**
     * Set the DeviceList
     */
    public void setHrmDeviceList(List<BluetoothDevice> list) {
        this.mDeviceList = list;
    }

    /**
     * Customize UI based on state of HRM device
     */
    public void setCurrentDeviceState(RowerConnectionStates state, int position) {
        this.mCurrentState = state;
        if (position != -1) {
            this.mSelectedPosition = position;
        }
    }

    /**
     * Set-up lottie animation
     */
    private void prepareLottieAnimationView(LottieAnimationView lottieView,
                                            String animationName, boolean loop) {
        lottieView.setAnimation(animationName);
        lottieView.loop(loop);
    }
}
