package com.android.ranit.beatmonitor.contract;

import android.widget.Button;

public interface MainActivityContract {
    interface View {
        void onConnectButtonClicked();
        void onDisconnectButtonClicked();
        void requestPermissions();
        boolean checkPermissionsAtRuntime();
        boolean checkBluetoothStatus();
        void displaySnackBar(String message);
        void playLottieAnimation(String animationName);
        void changeVisibility(android.view.View view, int visibility);
        void switchButton(Button button, String text);
        void launchAlertDialog();

        void startScanningForHrmDevices();
        void stopScanningForHrmDevices();

        void bindToBleService();
        void unBindFromBleService();

        void registerToBroadcastReceiver();
        void unregisterFromBroadcastReceiver();

        void connectToDevice(String address);
        void disconnectFromDevice();

//        void setHeartRateChart();
//        void setMaximumHeartRate();
    }
}
