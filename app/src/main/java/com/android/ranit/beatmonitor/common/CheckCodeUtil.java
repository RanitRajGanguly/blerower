package com.android.ranit.beatmonitor.common;

public class CheckCodeUtil {
    /**
     * 亦或校验
     * @param data
     * @return
     */
    public static byte[] getXor(byte[] data) {
        byte temp = data[1];
        for (int i = 2; i < data.length; i++) {
            temp ^= data[i];
        }
        byte[] data1 = new byte[]{temp,0x03};
        return byteMerger(data,data1);
    }

    /**
     * 字节数组合并
     * @param byte_1
     * @param byte_2
     * @return
     */
    public static byte[] byteMerger(byte[] byte_1, byte[] byte_2){
        byte[] byte_3 = new byte[byte_1.length+byte_2.length];
        System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
        System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
        return byte_3;
    }
}
