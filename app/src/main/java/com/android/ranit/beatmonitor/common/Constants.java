package com.android.ranit.beatmonitor.common;

public class Constants {
    public static final int ONE_SECOND_INTERVAL = 1000;

    public static final int REQUEST_PERMISSION_ALL = 100;
    public static final int REQUEST_PERMISSION_SETTING = 200;
    public static final int REQUEST_ENABLE_BLUETOOTH = 300;

    public static final String ZERO = "0";

    public static final int DURATION_HALF_SECOND = 500;
}
