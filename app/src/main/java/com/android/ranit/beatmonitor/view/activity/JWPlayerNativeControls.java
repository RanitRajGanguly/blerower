package com.android.ranit.beatmonitor.view.activity;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.android.ranit.beatmonitor.R;
import com.google.android.material.button.MaterialButton;
import com.longtailvideo.jwplayer.JWPlayerView;

public class JWPlayerNativeControls extends RelativeLayout {
    private static final String TAG = JWPlayerNativeControls.class.getSimpleName();

    private Context mContext;
    private JWPlayerView mPlayerView;
    private ImageView backIV;
    TextView rpmScoreTV, resistanceScoreTV, powerTV, rpmLabelTV, resistanceLabelTV, powerLabelTV, frequencyScoreTV;
    MaterialButton mConnectBtn;

    private JWPlayerInterface mInterface;
    private LottieAnimationView  mLottieAnimationProgressBar;

    public JWPlayerNativeControls(Context context, JWPlayerInterface mInterface) {
        this(context, null, 0);

        init(context);
        this.mInterface = mInterface;
        this.mContext = context;
    }

    public JWPlayerNativeControls(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init(context);
        this.mContext = context;
    }

    public JWPlayerNativeControls(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        this.mContext = context;
    }

    private void init(Context context) {
        View v = inflate(context, R.layout.native_controls, this);

        mLottieAnimationProgressBar = v.findViewById(R.id.lottieAnimationProgressBar);

        rpmScoreTV = v.findViewById(R.id.rpmScoreTV);
        resistanceScoreTV = v.findViewById(R.id.resistanceScoreTV);
        powerTV = v.findViewById(R.id.powerScoreTV);
        frequencyScoreTV = v.findViewById(R.id.frequencyScoreTV);

        rpmLabelTV = v.findViewById(R.id.rpmLabelTV);
        resistanceLabelTV = v.findViewById(R.id.resistanceLabelTV);
        powerLabelTV = v.findViewById(R.id.powerLabelTV);

        backIV = v.findViewById(R.id.backIV);
//        mConnectBtn = v.findViewById(R.id.btnStartScanning);

        initViews(context);

    }

    private void initViews(Context context) {
        backIV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterface.JWPlayerBackButton();
            }
        });

//        mConnectBtn.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mInterface.ConnectBtn();
//            }
//        });
    }

    public void setJWView(JWPlayerView playerView) {
        mPlayerView = playerView;
    }

    public void updateScoreData(String rpm, String resistance, String power, String frequency) {
        rpmScoreTV.setText(rpm);
        resistanceScoreTV.setText(resistance);
        powerTV.setText(power);
        frequencyScoreTV.setText(frequency);
    }

    private void calculateDistance(String rpm) {

        if(rpm != null) {

                int rpmValueFromSdk = Integer.parseInt(rpm);

                if(rpmValueFromSdk > 0) {

                    double distanceValue = 0.46 * 3.142 * rpmValueFromSdk / 7;
                    double distanceFinal = distanceValue / 1000;

                    Log.d("distance", String.format("%.2f", distanceFinal));

//                    distanceTV.setText(String.format("%.2f", distanceFinalValue));

                }

        }
    }

    public void updateDeviceInfo(String deviceName, String btnName) {

    }

    public interface JWPlayerInterface {
        void JWPlayerBackButton();
        void ConnectBtn();
    }

    public void showProgressBar(boolean isShown) {
        if (isShown) {
            mLottieAnimationProgressBar.setVisibility(View.VISIBLE);
        } else {
            mLottieAnimationProgressBar.setVisibility(View.INVISIBLE);
        }
    }
}
