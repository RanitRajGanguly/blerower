package com.android.ranit.beatmonitor.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.android.ranit.beatmonitor.R;
import com.android.ranit.beatmonitor.common.BicycleState;
import com.android.ranit.beatmonitor.common.BleGattAttributes;
import com.android.ranit.beatmonitor.common.CheckCodeUtil;
import com.android.ranit.beatmonitor.common.Constants;

import java.util.UUID;

public class BleConnectivityService extends Service {
    private static final String TAG = BleConnectivityService.class.getSimpleName();

    // Intent Filter actions for Broadcast-Receiver
    public final static String ACTION_GATT_CONNECTED = "fit.tread.treadone.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "fit.tread.treadone.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "fit.tread.treadone.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_ROWER_DATA = "fit.tread.treadone.ACTION_ROWER_DATA";
    public final static String ACTION_ROWER_METRICS = "fit.tread.treadone.ACTION_ROWER_METRICS";    // Gives Resistance, Speed, Power and Frequency

    // Services and Characteristics UUIDs
    private final UUID UUID_FITNESS_MACHINE_SERVICE = UUID.fromString(BleGattAttributes.FITNESS_MACHINE_SERVICE);
    private final UUID UUID_ROWER_METRICS_SERVICE = UUID.fromString(BleGattAttributes.ROWER_METRICS_SERVICE);
    private final UUID UUID_ROWER_DATA_CHARACTERISTIC = UUID.fromString(BleGattAttributes.ROWER_DATA_CHARACTERISTIC);
    private final UUID UUID_ROWER_METRICS_CHARACTERISTIC = UUID.fromString(BleGattAttributes.ROWER_READ_METRICS_CHARACTERISTIC);
    private final UUID UUID_ROWER_WRITE_CHARACTERISTIC = UUID.fromString(BleGattAttributes.ROWER_WRITE_CHARACTERISTIC);

    // Characteristic WRITE properties
    private final byte[] USER_INFO = new byte[]{BicycleState.ORDER_START, BicycleState.CarTableControl,
            BicycleState.CarTableControlUser,0,0,0,0,(byte)60,(byte)170,(byte)24,0};;

    private final IBinder mBinder = new LocalBinder();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mBluetoothGatt;
    private String mBluetoothDeviceAddress;
    private int mDeviceAddress;
    private String mSpeed = Constants.ZERO, mResistance = Constants.ZERO, mPower = Constants.ZERO,
            mFrequency = Constants.ZERO, mCalories = Constants.ZERO;

    /**
     * Binder for Service
     */
    public class LocalBinder extends Binder {
        public BleConnectivityService getService() {
            return BleConnectivityService.this;
        }
    }

    /**
     * Implementing GATT callback events
     */
    private final BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            Log.d(TAG, "onConnectionStateChange() called with newSate: " + newState);

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                broadcastUpdate(ACTION_GATT_CONNECTED,
                        gatt.getDevice().getName(), gatt.getDevice().getAddress());

                // Discover Services upon successful connection
                mBluetoothGatt.discoverServices();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                broadcastUpdate(ACTION_GATT_DISCONNECTED);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            Log.d(TAG, "onServicesDiscovered() called");

            if (status == BluetoothGatt.GATT_SUCCESS) {
                // Obtain required services from the List of services in the device
                BluetoothGattService serviceFitnessMachine = gatt.getService(UUID.fromString(BleGattAttributes.FITNESS_MACHINE_SERVICE));
                BluetoothGattService serviceRowerMetrics = gatt.getService(UUID.fromString(BleGattAttributes.ROWER_METRICS_SERVICE));

                if (serviceFitnessMachine.getUuid().equals(UUID_FITNESS_MACHINE_SERVICE)) {
                    Log.d(TAG, "onServicesDiscovered() called: Discovered Fitness-Machine Service");
                    BluetoothGattCharacteristic characteristic = serviceFitnessMachine.getCharacteristic(UUID.fromString(BleGattAttributes.ROWER_DATA_CHARACTERISTIC));
                    gatt.setCharacteristicNotification(characteristic, true);

                    BluetoothGattDescriptor descriptor =
                            characteristic.getDescriptor(UUID.fromString(BleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);

                    gatt.writeDescriptor(descriptor);
                }

                if (serviceRowerMetrics.getUuid().equals(UUID_ROWER_METRICS_SERVICE)) {
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "onServicesDiscovered() called: Discovered Rower-Machine Service");
                            BluetoothGattCharacteristic readRowerMetricsCharacteristic = serviceRowerMetrics.getCharacteristic(UUID.fromString(BleGattAttributes.ROWER_READ_METRICS_CHARACTERISTIC));

                            gatt.setCharacteristicNotification(readRowerMetricsCharacteristic, true);

                            BluetoothGattDescriptor descriptor1 =
                                    readRowerMetricsCharacteristic.getDescriptor(UUID.fromString(BleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));

                            descriptor1.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                            gatt.writeDescriptor(descriptor1);

                            // Broadcast update
                            broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                        }
                    }, Constants.DURATION_HALF_SECOND);
                }

            } else {
                Log.e(TAG, "onServicesDiscovered() received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            Log.d(TAG, "onCharacteristicRead() called");
            if (status == BluetoothGatt.GATT_SUCCESS) {
                // readBleCharacteristic(characteristic);
                broadcastUpdate(ACTION_ROWER_DATA, characteristic);
            }
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Log.d(TAG, "onDescriptorWrite: Called");
            BluetoothGattCharacteristic characteristic =
                    gatt.getService(UUID.fromString(BleGattAttributes.ROWER_METRICS_SERVICE))
                            .getCharacteristic(UUID.fromString(BleGattAttributes.ROWER_WRITE_CHARACTERISTIC));

            // Writing data to Characteristic with UUID 0xFFF2 to the device
            if (characteristic.getUuid().equals(UUID_ROWER_WRITE_CHARACTERISTIC)) {
                characteristic.setValue(CheckCodeUtil.getXor(USER_INFO));
                boolean writeStatus = gatt.writeCharacteristic(characteristic);

                Log.e(TAG, "Characteristic Write-Status = [" + writeStatus + "]");
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            Log.d(TAG, "onCharacteristicWrite() called with: characteristic = [" + characteristic + "], status = [" + status + "]");

            // Print response from device upon successful 'onCharacteristicWrite' for UUID 0xFFF2
            if (characteristic.getUuid().equals(UUID_ROWER_WRITE_CHARACTERISTIC)) {
                final byte[] data = characteristic.getValue();
                if (data != null && data.length > 0) {
                    final StringBuilder stringBuilder = new StringBuilder(data.length);

                    for (byte byteChar : data) {
                        int rawByteToInt = Integer.parseInt(String.valueOf(byteChar));
                        int currentInt = rawByteToInt & 0xFF;

                        stringBuilder.append(" ").append(currentInt);
                    }
                    Log.d(TAG, "Response from Rower: "+ stringBuilder.toString());
                }
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Log.d(TAG, "onCharacteristicChanged() called");

            if (characteristic.getUuid().equals(UUID_ROWER_METRICS_CHARACTERISTIC)) {
                broadcastUpdate(ACTION_ROWER_METRICS, characteristic);
            }

            if (characteristic.getUuid().equals(UUID_ROWER_DATA_CHARACTERISTIC)) {
                broadcastUpdate(ACTION_ROWER_DATA, characteristic);
            }
        }
    };

    /**
     * Send broadcast for the current GATT status
     * @param action - Intent for broadcast receiver
     */
    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, String deviceName, String deviceAddress) {
        final Intent intent = new Intent(action);
        intent.putExtra(getString(R.string.device_name), deviceName);
        intent.putExtra(getString(R.string.device_address), deviceAddress);
        sendBroadcast(intent);
    }
    /**
     * Formats byte[] data for the current GATT status and updates UI via Broadcast
     * @param action - Intent for broadcast receiver
     */
    private void broadcastUpdate(final String action, final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        final byte[] data = characteristic.getValue();
        if (data != null && data.length > 7) {
            if (action.equals(ACTION_ROWER_DATA)) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);

                // Format Calories
                formatCaloriesData(data);

                // Send data via intent to Broadcast-Receiver
                intent.putExtra(getString(R.string.rower_calories), mCalories);

                // Rower Data
                if (data[0] == 44) {
                    for (byte byteChar : data) {
                        int rawByteToInt = Integer.parseInt(String.valueOf(byteChar));
                        int currentInt = rawByteToInt & 0xFF;

                        stringBuilder.append(" ").append(currentInt);
                    }
                }
                Log.d(TAG, "Rower-Data: "+stringBuilder.toString());
            }

            if (action.equals(ACTION_ROWER_METRICS)) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);

                // Format data for UI
                formatRowerData(data);

                // Send data via intent to Broadcast-Receiver
                intent.putExtra(getString(R.string.rower_speed), mSpeed);
                intent.putExtra(getString(R.string.rower_resistance), mResistance);
                intent.putExtra(getString(R.string.rower_power), mPower);
                intent.putExtra(getString(R.string.rower_frequency), mFrequency);

                if (data[0] == 2) {
                    // Metrics Data
                    for (byte byteChar : data) {
                        int rawByteToInt = Integer.parseInt(String.valueOf(byteChar));
                        int currentInt = rawByteToInt & 0xFF;

                        stringBuilder.append(" ").append(currentInt);
                    }
                    Log.d(TAG, "Rower-Metrics: "+stringBuilder.toString());
                }

                // Broadcast the Intent
                sendBroadcast(intent);
            }
        }
    }

    /**
     * Formats the raw byte data from the Rower
     * @param data - byte[]
     */
    private void formatRowerData(byte[] data) {
        float currentInt2 = data[3] & 0xFF;
        float currentInt1 = data[4] & 0xFF;

        mSpeed = String.valueOf((currentInt2 + currentInt1 * 256) / 100);
        mResistance = String.valueOf((int) data[5]);

        currentInt2 = data[6] & 0xFF;
        currentInt1 = data[7] & 0xFF;
        mFrequency = String.valueOf(((int) currentInt2 + (int) currentInt1 * 256));

        currentInt2 = data[9] & 0xFF;
        currentInt1 = data[10] & 0xFF;
        mPower = String.valueOf(((int) currentInt2 + (int) currentInt1 * 256) / 10);
    }

    /**
     * Formats the raw byte data OF CALORIES from the Rower
     * @param data
     */
    private void formatCaloriesData(byte[] data) {
        int currentInt2 = data[8] & 0xFF;
        int currentInt1 = data[9] & 0xFF;
        mCalories = String.valueOf((currentInt2 + currentInt1 * 256));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate() called");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind() called");
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind() called");
        closeBluetoothGatt();
        return super.onUnbind(intent);
    }

    /**
     * Initialize reference to the Bluetooth Adapter (from Bluetooth Manager)
     *
     * @return boolean - Adapter initialization status
     */
    public boolean initializeBluetoothAdapter() {
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

            if (mBluetoothManager == null) {
                Log.e(TAG, "initializeBluetoothAdapter: Bluetooth Manager couldn't be initialized");
                return false;
            }

            mBluetoothAdapter = mBluetoothManager.getAdapter();
            if (mBluetoothAdapter == null) {
                Log.e(TAG, "initializeBluetoothAdapter: Bluetooth Adapter couldn't be initialized");
                return false;
            }
        }
        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address - The device address of the destination device.
     * @return boolean - true if the connection is initiated successfully.
     */
    public boolean connectToBleDevice(String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.e(TAG, "connectToBleDevice: BluetoothAdapter not initialized or unspecified address");
            return false;
        }

        // Reconnecting to previously connected device
        if (address.equals(mBluetoothDeviceAddress) && mBluetoothGatt != null) {
            Log.d(TAG, "connectToBleDevice: Reconnecting to device");
            if (mBluetoothGatt.connect()) {
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.e(TAG, "connectToBleDevice: Device not found");
            return false;
        }

        Log.d(TAG, "connectToBleDevice: Initiating Connection with device: " + device.getName());
        mBluetoothGatt = device.connectGatt(this, false, bluetoothGattCallback);

        mBluetoothDeviceAddress = address;
        return true;
    }

    /**
     * Disconnects from the existing connection or cancels a pending connection
     */
    public void disconnectFromBleDevice() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.e(TAG, "disconnectFromBleDevice: BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * Read the characteristic
     *
     * @param characteristic - bluetoothGattCharacteristic which is to be read
     */
    private void readBleCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.e(TAG, "readBleCharacteristic: BluetoothAdapter not initialized");
            return;
        }

        Log.d(TAG, "readBleCharacteristic: Reading Characteristic");
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Must be called after using BLE to ensure proper release of resources
     */
    public void closeBluetoothGatt() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }
}
