package com.android.ranit.beatmonitor.view.activity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.ranit.beatmonitor.R;
import com.android.ranit.beatmonitor.common.BleGattAttributes;
import com.android.ranit.beatmonitor.common.Constants;
import com.android.ranit.beatmonitor.common.RowerConnectionStates;
import com.android.ranit.beatmonitor.contract.MainActivityContract;
import com.android.ranit.beatmonitor.databinding.ActivityDemoPlayerBinding;
import com.android.ranit.beatmonitor.service.BleConnectivityService;
import com.android.ranit.beatmonitor.view.adapter.BleDeviceAdapter;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.longtailvideo.jwplayer.configuration.PlayerConfig;
import com.longtailvideo.jwplayer.events.BufferEvent;
import com.longtailvideo.jwplayer.events.CompleteEvent;
import com.longtailvideo.jwplayer.events.ErrorEvent;
import com.longtailvideo.jwplayer.events.FirstFrameEvent;
import com.longtailvideo.jwplayer.events.IdleEvent;
import com.longtailvideo.jwplayer.events.PlayEvent;
import com.longtailvideo.jwplayer.events.ReadyEvent;
import com.longtailvideo.jwplayer.events.listeners.VideoPlayerEvents;
import com.longtailvideo.jwplayer.media.playlists.PlaylistItem;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class JWPlayerActivity extends AppCompatActivity implements VideoPlayerEvents.OnReadyListener, VideoPlayerEvents.OnFirstFrameListener,
        VideoPlayerEvents.OnCompleteListener, VideoPlayerEvents.OnIdleListener, VideoPlayerEvents.OnErrorListener, VideoPlayerEvents.OnPlayListener,
        VideoPlayerEvents.OnBufferListener, JWPlayerNativeControls.JWPlayerInterface, MainActivityContract.View {

    private static final String TAG = JWPlayerActivity.class.getSimpleName();

    private ActivityDemoPlayerBinding mBinding;
    private JWPlayerNativeControls mNativePlayerControls;
    private String videoFileName;


    private final UUID ROWER_SERVICE_UUID = UUID.fromString(BleGattAttributes.FITNESS_MACHINE_SERVICE);
    private final UUID[] mHeartRateServiceUuids = new UUID[]{ROWER_SERVICE_UUID};

    private static final String SCANNING = "scanning.json";
    private static final String START_SCAN = "start_scan.json";

    private final String[] PERMISSIONS = {
            // Note: Only 'ACCESS_FINE_LOCATION' permission is needed
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private List<ScanFilter> mBluetoothLeScanFilter = null;
    private BleConnectivityService mService;

    private View mCustomAlertView;
    private RecyclerView mRecyclerView;
    private LottieAnimationView mScanningLottieView;
    private BleDeviceAdapter mRvAdapter;
    private Intent mServiceIntent;

    private final List<BluetoothDevice> mBleDeviceList = new ArrayList<>();
    private RowerConnectionStates mCurrentState = RowerConnectionStates.DISCONNECTED;

    private String mDeviceName;
    private String mDeviceAddress;
    private String mSpeed = Constants.ZERO, mDistance = Constants.ZERO, mCalories = Constants.ZERO,
            mResistance = Constants.ZERO, mFrequency = Constants.ZERO, mPower = Constants.ZERO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_demo_player);

        mNativePlayerControls = new JWPlayerNativeControls(JWPlayerActivity.this, this);

        mNativePlayerControls.showProgressBar(true);
        //BIKE
        videoFileName = "https://cdn.jwplayer.com/manifests/" + "oQ5Fr8l1" + ".m3u8";
        //TREADMILL
        videoFileName = "https://cdn.jwplayer.com/manifests/" + "s5kT1oXv" + ".m3u8";
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mServiceIntent = new Intent(this, BleConnectivityService.class);
        bindToBleService();
        onConnectButtonClicked();

    }

    private void startPlayer() {
        //Setup our player with a demo playlist item so it plays a video
        PlaylistItem playlistItem = new PlaylistItem.Builder()
                .file(videoFileName)
                .build();
        List<PlaylistItem> playlist = new ArrayList<>();
        playlist.add(playlistItem);
        PlayerConfig config = new PlayerConfig.Builder().playlist(playlist).build();
        mBinding.jwplayer.setup(config);

        //Disable the player's controls so we don't have conflict with our native ones
        mBinding.jwplayer.setControls(false);
        mBinding.jwplayer.setFullscreen(true, false);

        // Add Listeners
        mBinding.jwplayer.addOnReadyListener(this);
        mBinding.jwplayer.addOnFirstFrameListener(this);
        mBinding.jwplayer.addOnIdleListener(this);
        mBinding.jwplayer.addOnPlayListener(this);
        mBinding.jwplayer.addOnBufferListener(this);
        mBinding.jwplayer.addOnCompleteListener(this);
        mBinding.jwplayer.addOnErrorListener(this);

        //Initialize our mNativePlayerControls
        mNativePlayerControls.setJWView(mBinding.jwplayer);
        mBinding.jwplayer.addView(mNativePlayerControls);
    }

    private void removePlayerListener() {
        mBinding.jwplayer.removeOnReadyListener(this);
        mBinding.jwplayer.removeOnFirstFrameListener(this);
        mBinding.jwplayer.removeOnIdleListener(this);
        mBinding.jwplayer.removeOnPlayListener(this);
        mBinding.jwplayer.removeOnBufferListener(this);
        mBinding.jwplayer.removeOnCompleteListener(this);
        mBinding.jwplayer.removeOnErrorListener(this);
    }

    @Override
    public void JWPlayerBackButton() {
        finish();
    }

    @Override
    public void ConnectBtn() {
        Log.d(TAG, "onConnectButtonClicked() called");
        prepareAlertDialog();

        // Start Scanning prior to launch
        startScanningForHrmDevices();
        launchAlertDialog();

    }

    @Override
    public void onReady(ReadyEvent readyEvent) {
        if (readyEvent != null) {
            Log.d(TAG, "onReady() called with: setup-time = [" + readyEvent.getSetupTime() + "]");
        }
        mBinding.jwplayer.play();
    }

    @Override
    public void onBuffer(BufferEvent bufferEvent) {
        Log.d(TAG, "onBuffer() called");
        mNativePlayerControls.showProgressBar(true);
    }

    @Override
    public void onPlay(PlayEvent playEvent) {
        Log.d(TAG, "onPlay() called");
        mNativePlayerControls.showProgressBar(false);
    }

    @Override
    public void onFirstFrame(FirstFrameEvent firstFrameEvent) {
        if (firstFrameEvent != null) {
            Log.d(TAG, "onFirstFrame() called with: firstFrameEvent = [" + firstFrameEvent.getLoadTime() + "]");
        }
    }

    @Override
    public void onIdle(IdleEvent idleEvent) {
        Log.d(TAG, "onIdle() called");
    }

    @Override
    public void onComplete(CompleteEvent completeEvent) {
        Log.d(TAG, "onComplete() called");
    }

    @Override
    public void onError(ErrorEvent errorEvent) {
        Log.d(TAG, "onError() called");
        if (errorEvent != null) {
            if (errorEvent.getException() != null) {
                Log.d(TAG, errorEvent.getException().getMessage());
            }
        }
        mNativePlayerControls.showProgressBar(false);

        Toast.makeText(JWPlayerActivity.this, "We are experiencing some error, Sorry for the inconvenience", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkPermissionsAtRuntime()) {
            if (!checkBluetoothStatus()) {
                enableBluetoothRequest();
            }
        }

        mBinding.jwplayer.onResume();
        mBinding.jwplayer.play();

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        mBinding.jwplayer.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.jwplayer.onStart();
        requestPermissions();
        registerToBroadcastReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");

        mBinding.jwplayer.onStop();
        unregisterFromBroadcastReceiver();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBinding.jwplayer.onDestroy();
        removePlayerListener();
        unBindFromBleService();
    }

    /**
     * Manage Service life-cycles
     */
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mService = ((BleConnectivityService.LocalBinder) service).getService();
            if (!mService.initializeBluetoothAdapter()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
            }
            // Automatically connects to the device upon successful start-up initialization.
            mService.connectToBleDevice(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };

    /**
     * Broadcast Receiver to communicate and update UI components from Service
     */
    private final BroadcastReceiver mGattUpdateBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (BleConnectivityService.ACTION_GATT_CONNECTED.equals(action)) {
                Log.d(TAG, "onReceive: ACTION_GATT_CONNECTED");
                mCurrentState = RowerConnectionStates.CONNECTED;

                mDeviceName = intent.getStringExtra("DEVICE_NAME");
                mDeviceAddress = intent.getStringExtra("DEVICE_ADDRESS");

                onConnectBroadcast();
                onDisconnectButtonClicked();
                updateAdapterConnectionState(-1);

            } else if (BleConnectivityService.ACTION_GATT_DISCONNECTED.equals(action)) {
                Log.e(TAG, "onReceive: ACTION_GATT_DISCONNECTED");
                mCurrentState = RowerConnectionStates.DISCONNECTED;

                onDisconnectBroadcast();
                onConnectButtonClicked();
                updateAdapterConnectionState(-1);

            } else if (BleConnectivityService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                Log.d(TAG, "onReceive: ACTION_GATT_SERVICES_DISCOVERED");

            } else if (BleConnectivityService.ACTION_ROWER_METRICS.equals(action)) {
//                Log.d(TAG, "onReceive: ACTION_DATA_AVAILABLE");

                //bike data
//                rpm = intent.getStringExtra("rpm");
//                resistance = intent.getStringExtra("resistance");
//                power = intent.getStringExtra("power");

//                mNativePlayerControls.updateScoreData(rpm, resistance, power, false);

                //treadmill data
                mSpeed = intent.getStringExtra(getString(R.string.rower_speed));
                mResistance = intent.getStringExtra(getString(R.string.rower_resistance));
                mPower = intent.getStringExtra(getString(R.string.rower_power));
                mFrequency = intent.getStringExtra(getString(R.string.rower_frequency));

                // Update Rower data
                mNativePlayerControls.updateScoreData(mSpeed, mResistance, mPower, mFrequency);
            }
        }
    };

    @Override
    public void onConnectButtonClicked() {
        Log.d(TAG, "onConnectButtonClicked() called");
        mBinding.btnStartScanning.setOnClickListener(connectButtonClickListener);
    }

    @Override
    public void onDisconnectButtonClicked() {
        Log.d(TAG, "onDisconnectButtonClicked() called");
        mBinding.btnStartScanning.setOnClickListener(disconnectButtonClickListener);
    }

    @Override
    public void requestPermissions() {
        Log.d(TAG, "requestPermissions() called");
        ActivityCompat.requestPermissions(this, PERMISSIONS, Constants.REQUEST_PERMISSION_ALL);
    }

    @Override
    public boolean checkPermissionsAtRuntime() {
        Log.d(TAG, "checkPermissionsAtRuntime() called");
        for (String permission : PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean checkBluetoothStatus() {
        Log.d(TAG, "checkBluetoothStatus() called");
        if (mBluetoothAdapter != null) {
            // Return Bluetooth Enable Status
            return mBluetoothAdapter.isEnabled();
        } else {
//            displaySnackBar("This device doesn't support Bluetooth");
            return false;
        }
    }

    @Override
    public void displaySnackBar(String message) {
        Snackbar.make(mBinding.layout, message, Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void playLottieAnimation(String animationName) {
        mScanningLottieView.setAnimation(animationName);
        mScanningLottieView.playAnimation();
    }

    @Override
    public void changeVisibility(View view, int visibility) {
        view.setVisibility(visibility);
    }

    @Override
    public void switchButton(Button button, String text) {
        Log.d(TAG, "switchButton() called with: text = [" + text + "]");
        button.setText(text);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull @org.jetbrains.annotations.NotNull String[] permissions, @NonNull @org.jetbrains.annotations.NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean isAlertDialogInflated = false;
        if (requestCode == Constants.REQUEST_PERMISSION_ALL) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        // Called when user selects 'NEVER ASK AGAIN'
                        isAlertDialogInflated = true;

                    } else {
                        // Called when user selects 'DENY'
                        displaySnackBar("Enable permission");
                    }
                }
            }
            inflateEnablePermissionDialog(isAlertDialogInflated);
        }
    }

    /**
     * Shows Alert Dialog when User denies permission permanently
     *
     * @param isTrue - true when user selects on never-ask-again
     */
    private void inflateEnablePermissionDialog(boolean isTrue) {
        if (isTrue) {
            // Inflate Alert Dialog
            new MaterialAlertDialogBuilder(this)
                    .setTitle("Permissions Mandatory")
                    .setMessage("Kindly enable all permissions through Settings")
                    .setPositiveButton("OKAY", (dialogInterface, i) -> {
                        launchAppSettings();
                        dialogInterface.dismiss();
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    /**
     * Launch App-Settings Screen
     */
    private void launchAppSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, Constants.REQUEST_PERMISSION_SETTING);
    }

    /**
     * Launch Enable Bluetooth Request
     */
    private void enableBluetoothRequest() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BLUETOOTH);
    }

    /**
     * Click Listener for Connect Button
     */
    private final View.OnClickListener connectButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // Launch Custom Material Alert-Dialog
            prepareAlertDialog();

            // Start Scanning prior to launch
            startScanningForHrmDevices();
            launchAlertDialog();
        }
    };

    /**
     * Click Listener for disconnect button
     */
    private final View.OnClickListener disconnectButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // Disconnect from device
            disconnectFromDevice();
        }
    };

    /**
     * Prepare Custom Alert-Dialog
     */
    private void prepareAlertDialog() {
        // Inflate custom layout
        mCustomAlertView = LayoutInflater.from(this)
                .inflate(R.layout.dialog_ble_device_scanning, null, false);

        mRecyclerView = mCustomAlertView.findViewById(R.id.rvScannedBleDevices);
        mScanningLottieView = mCustomAlertView.findViewById(R.id.lottieAvScanning);

        displayDataInRecyclerView(mRecyclerView);
    }

    /**
     * Launch Custom Alert-Dialog to start/stop scanning
     */
    @Override
    public void launchAlertDialog() {
        Log.d(TAG, "launchAlertDialog() called");
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(mCustomAlertView)
                .setTitle(R.string.dialog_title)
                .setMessage(R.string.dialog_message)
                .setPositiveButton(R.string.dialog_positive_button, null)
                .setNegativeButton(R.string.dialog_negative_button, null)
                .setNeutralButton(R.string.dialog_neutral_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        stopScanningForHrmDevices();
                        dialogInterface.dismiss();
                    }
                })
                .setCancelable(false)
                .create();

        // Implemented in order to avoid auto-dismiss upon click of a dialog button
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button positiveButton = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                Button negativeButton = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE);

                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "Start button clicked");
                        startScanningForHrmDevices();
                    }
                });

                negativeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "Stop button clicked");
                        stopScanningForHrmDevices();
                    }
                });
            }
        });
        dialog.show();
    }

    /**
     * Prepare RecyclerView adapter
     * @param recyclerView
     */
    private void displayDataInRecyclerView(RecyclerView recyclerView) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        // Set-up adapter
        mRvAdapter = new BleDeviceAdapter(this, mBleDeviceList,
                new BleDeviceAdapter.HrmDeviceItemClickListener() {
                    @Override
                    public void onHrmDeviceClicked(int position) {
                        if (mCurrentState == RowerConnectionStates.CONNECTED) {
                            // Disconnect
                            Log.d(TAG, "Disconnecting from Device: "+mBleDeviceList.get(position).getName());
                            disconnectFromDevice();
                            mCurrentState = RowerConnectionStates.DISCONNECTING;
                            updateAdapterConnectionState(position);
                        } else if (mCurrentState == RowerConnectionStates.DISCONNECTED) {
                            // Connect
                            Log.d(TAG, "Connecting to Device: "+mBleDeviceList.get(position).getName());
                            connectToDevice(mBleDeviceList.get(position).getAddress());
                            mCurrentState = RowerConnectionStates.CONNECTING;
                            updateAdapterConnectionState(position);
                            startPlayer();
                        }
                    }
                });

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mRvAdapter);
    }

    /**
     * Step 1 of 3: Start Scanning for BLE Devices
     *
     * Scanning requires 3 parameters to 'Start Scanning':
     * a) ScanFilter (pass 'null' in case no-specific filtering is required)
     * b) ScanSettings
     * c) ScanCallback
     */
    @Override
    public void startScanningForHrmDevices() {
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();

        if (mScanningLottieView.getVisibility() != View.VISIBLE) {
            changeVisibility(mScanningLottieView, View.VISIBLE);
        }
        playLottieAnimation(SCANNING);

        // Begin Scan
        Log.d(TAG, "Started Scanning for HRM devices");
        mBluetoothLeScanner.startScan(hrmScanFilter(), bluetoothLeScanSettings, bluetoothLeScanCallback);
    }

    /**
     * Scanning consumes a lot of battery resource.
     * Hence, stopScan is mandatory
     *
     * 'stopScan' requires one 1 parameter (i.e) 'ScanCallback'
     */
    @Override
    public void stopScanningForHrmDevices() {
        Log.d(TAG, "stopScanningForHrmDevices() called");

        if (mScanningLottieView.getVisibility() != View.VISIBLE) {
            changeVisibility(mScanningLottieView, View.VISIBLE);
            changeVisibility(mRecyclerView, View.GONE);
        }
        playLottieAnimation(START_SCAN);

        mBluetoothLeScanner.stopScan(bluetoothLeScanCallback);
        mBleDeviceList.clear();
        mRvAdapter.notifyDataSetChanged();
    }

    @Override
    public void bindToBleService() {
        Log.d(TAG, "bindToBleService() called");
        bindService(mServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    public void unBindFromBleService() {
        Log.d(TAG, "unBindFromBleService() called");
        unbindService(mServiceConnection);
    }

    @Override
    public void registerToBroadcastReceiver() {
        Log.d(TAG, "registerToBroadcastReceiver() called");
        registerReceiver(mGattUpdateBroadcastReceiver, makeGattUpdateIntentFilter());
    }

    @Override
    public void unregisterFromBroadcastReceiver() {
        Log.d(TAG, "unregisterFromBroadcastReceiver() called");
        unregisterReceiver(mGattUpdateBroadcastReceiver);
    }

    @Override
    public void connectToDevice(String address) {
        Log.d(TAG, "connectToDevice() called");
        mService.connectToBleDevice(address);
    }

    @Override
    public void disconnectFromDevice() {
        Log.d(TAG, "disconnectFromDevice() called");
        mService.disconnectFromBleDevice();
    }

    /**
     * Preparing 'ScanFilter' specific to HRM devices
     */
    private List<ScanFilter> hrmScanFilter() {
        if(mHeartRateServiceUuids != null) {
            mBluetoothLeScanFilter = new ArrayList<>();
            for (UUID heartRateServiceUUID : mHeartRateServiceUuids) {
                ScanFilter filter = new ScanFilter.Builder()
                        .setServiceUuid(new ParcelUuid(heartRateServiceUUID))
                        .build();
                mBluetoothLeScanFilter.add(filter);
            }
        }
        return mBluetoothLeScanFilter;
    }

    /**
     * Initializing 'ScanSettings' parameter for 'BLE device Scanning' via Builder Pattern
     */
    private final ScanSettings bluetoothLeScanSettings = new ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
            .setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
            .setNumOfMatches(ScanSettings.MATCH_NUM_ONE_ADVERTISEMENT)
            .build();

    /**
     * Initializing 'ScanCallback' parameter for 'BLE device Scanning'
     *
     * NOTE: onScanResult is triggered whenever a BLE device, matching the
     *       ScanFilter and ScanSettings is found.
     *       In this callback, we get access to the BluetoothDevice and RSSI
     *       objects through the ScanResult
     */
    private final ScanCallback bluetoothLeScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            BluetoothDevice bluetoothDevice = result.getDevice();

            // Append device to Scanned devices list
            if (bluetoothDevice.getName() != null) {
                if (!mBleDeviceList.contains(bluetoothDevice)) {
                    Log.d(TAG, "onScanResult: Adding "+bluetoothDevice.getName()+" to list");
                    mBleDeviceList.add(bluetoothDevice);

                    changeVisibility(mRecyclerView, View.VISIBLE);
                    changeVisibility(mScanningLottieView, View.GONE);

                    mRvAdapter.setHrmDeviceList(mBleDeviceList);
                    mRvAdapter.notifyDataSetChanged();
                }
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.e(TAG, "onScanFailed() called with: errorCode = [" + errorCode + "]");
        }
    };

    /**
     * Prepare Intent Filters for GATT Update Status
     *
     * @return intentFilter
     */
    private IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BleConnectivityService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BleConnectivityService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BleConnectivityService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BleConnectivityService.ACTION_ROWER_DATA);
        intentFilter.addAction(BleConnectivityService.ACTION_ROWER_METRICS);
        return intentFilter;
    }

    /**
     * Update Current connection state to Adapter
     */
    private void updateAdapterConnectionState(int position) {
        mRvAdapter.setCurrentDeviceState(mCurrentState, position);
        mRvAdapter.notifyDataSetChanged();
    }

    /**
     * Update UI when DISCONNECT Broadcast is received
     */
    private void onDisconnectBroadcast() {
        mBinding.tvDeviceName.setText("No Device Connected");
        mBinding.tvConnectivityStatus.setText(R.string.disconnected);
        mBinding.tvConnectivityStatus.setTextColor(getResources().getColor(R.color.red_500));
        switchButton(mBinding.btnStartScanning, getResources().getString(R.string.connect));

        String mButtonName = getResources().getString(R.string.connect);
        mNativePlayerControls.updateDeviceInfo(mDeviceName, mButtonName);
    }

    /**
     * Update UI when CONNECT Broadcast is received
     */
    private void onConnectBroadcast() {
        mCurrentState = RowerConnectionStates.CONNECTED;
        mBinding.tvDeviceName.setText(mDeviceName);
        mBinding.tvConnectivityStatus.setText(R.string.connected);
        mBinding.tvConnectivityStatus.setTextColor(getResources().getColor(R.color.green_500));
        switchButton(mBinding.btnStartScanning, getResources().getString(R.string.disconnect));

//        String mButtonName = getResources().getString(R.string.disconnect);
//        mNativePlayerControls.updateDeviceInfo(mDeviceName, mButtonName);
    }

}