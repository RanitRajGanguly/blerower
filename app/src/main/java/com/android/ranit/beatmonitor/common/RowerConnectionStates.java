package com.android.ranit.beatmonitor.common;

public enum RowerConnectionStates {
    CONNECTING,
    CONNECTED,
    DISCONNECTING,
    DISCONNECTED
}
