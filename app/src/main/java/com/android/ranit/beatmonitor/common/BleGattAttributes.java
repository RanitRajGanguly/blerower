package com.android.ranit.beatmonitor.common;

public class BleGattAttributes {
    // SERVICES
    public static String FITNESS_MACHINE_SERVICE = "00001826-0000-1000-8000-00805f9b34fb";
    public static String ROWER_METRICS_SERVICE = "0000fff0-0000-1000-8000-00805f9b34fb";

    // CHARACTERISTICS
    public static String ROWER_DATA_CHARACTERISTIC = "00002ad1-0000-1000-8000-00805f9b34fb";
    public static String ROWER_READ_METRICS_CHARACTERISTIC = "0000fff1-0000-1000-8000-00805f9b34fb";
    public static String ROWER_WRITE_CHARACTERISTIC = "0000fff2-0000-1000-8000-00805f9b34fb";

    // CLIENT NOTIFICATION CHANNEL
    public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
}
